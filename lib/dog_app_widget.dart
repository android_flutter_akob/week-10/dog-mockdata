import 'package:dog_app/dog_form_widget.dart';
import 'package:flutter/material.dart';
import 'dog.dart';
import 'dog_service.dart';

class DogApp extends StatefulWidget {
  DogApp({Key? key}) : super(key: key);

  @override
  _DogAppState createState() => _DogAppState();
}

class _DogAppState extends State<DogApp> {
  late Future<List<Dog>> _dogs;
  @override
  void initState() {
    super.initState();
    _dogs = getDogs();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Dog App"),
      ),
      body: FutureBuilder(
        future: _dogs,
        builder: (context, snapshot) {
          List<Dog> dogs = snapshot.data as List<Dog>;
          if (snapshot.hasError) {
            return Text("Error!!");
          }
          return ListView.builder(
            itemBuilder: (context, index) {
              var dog = dogs.elementAt(index);
              return ListTile(
                title: Text(dog.name),
                subtitle: Text('id : ${dog.id} age : ${dog.age}'),
                trailing: IconButton(
                  icon: Icon(Icons.delete),
                  onPressed: () async {
                    await delDog(dog);
                    setState(() {
                      _dogs = getDogs();
                    });
                  },
                ),
                onTap: () async {
                  await Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => DogForm(dog: dog)));
                  setState(() {
                    _dogs = getDogs();
                  });
                },
              );
            },
            itemCount: dogs.length,
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () async {
          Dog newDog = Dog(id: -1, name: "", age: 0);
          await Navigator.push(context,
              MaterialPageRoute(builder: (context) => DogForm(dog: newDog)));

          setState(() {
            _dogs = getDogs();
          });
        },
      ),
    );
  }
}
